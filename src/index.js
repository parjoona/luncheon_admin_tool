import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import GraphQLProvider from './utils/GraphQLProvider';
// import {Auth0Provider} from './utils/react-auth0-spa.js';

import dotenv from 'dotenv';

dotenv.config()

// const onRedirectCallback = appState => {
//   window.history.replaceState(
//     {},
//     document.title,
//     appState && appState.targetUrl
//       ? appState.targetUrl
//       : window.location.pathname
//   );
// };

ReactDOM.render(
  // <Auth0Provider
  //   domain={process.env.REACT_APP_DOMAIN}
  //   client_id={process.env.REACT_APP_CLIENT_ID}
  //   audience={process.env.REACT_APP_AUDIENCE}
  //   redirect_uri={window.location.origin}
  //   onRedirectCallback={onRedirectCallback}
  // >
    <GraphQLProvider>
      <App/>
    </GraphQLProvider>
  // </Auth0Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
