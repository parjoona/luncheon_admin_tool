
# Introduction
 
This application is being built to digitalize the payment process for employees and finance department in Netcompany Norway.  
The administration application built using React 16.8+. As of now, this is a one-page application til further development. We chose to use react because of earlier experiance and increased react when using a web-application.  

Per today, signing up for lunch requires each individual to write their name on a paper, and later collected by finance.  
Therefore finance requires to count each individual, and manually input these numbers for the system.

# Pre-requisites

The solution is build with react.  
This project is using version 16.8 and above.

To run the project, there is a requirement to use Node.  
Installation and download for node can be found: [here](https://nodejs.org/en/download/)  

# Installation

This will show the easy steps how to install the application.

```
navigate to Luncheon_admin_tool/
```

Installation of packages
```
yarn install / npm install
```

Run the application on [localhost](http://localhost:3000/)  
```
yarn start / npm run start
```

# Usage

To make use of the full application, you will be required to run the backend  
The documentation for this can be found [here]()

### Using a local server

At the current moment, there is setup to connect to localhost with port 5000 when running the project locally.  
This can be configured in the utils/graphQLprovider

# Deployment

Right now, the application is deployed at a [Netlify](https://www.netlify.com) address.  
The solution is being pipelined from the master branch and rebuilds whenever a pull request gets accepted.

Application > [Luncheon](https://jolly-carson-684df7.netlify.com/)

# Further work

Further work can be seen on [Trello](https://trello.com/b/AZtPpVOh/netcompany-lunsj)